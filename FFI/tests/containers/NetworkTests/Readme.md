
## This Test Suite include 3 tests (6 fmf):

Setup for which includes installing podman, pulling the image and then build the image using relevant epel, iputils and sysstat installation.

1. test_icmp.sh -> tc01_network_stress_send_large_packets_icmp part of test.fmf:
Two containers running in a vm confusion and orderly:
Firstly, Orderly pings partner container 10 times in ideal conditions (time noted) and then confusion sends large size packets using  `ping -s` while orderly again pings partner container 10 times (while in network stress due to confusion container)time noted again and compared with time taken in ideal conditions.The metrics such as Packet delay, loss and errors are monitored using ping and sar. Evaluations present are - time delay should not be more than 5% when orderly pings partner container under stress and packet loss should not be more than 0%.

2. test_icmp.sh ->tc02_network_stress_ping_flood_icmp part of test.fmf:
Two containers running in a vm confusion and orderly:
Firstly, Orderly pings partner container 10 times in ideal conditions (time noted) and then confusion creates a ping storm using  `ping -f` while orderly again pings partner conrtainer 10 times (while in network stress or ping flood from confusion container)time noted again and compared with time taken in ideal conditions. The metrics such as Packet delay, loss and errors are monitored using ping and sar. Evaluations present are - time delay should not be more than 5% when orderly pings partner container under stress and packet loss should not be more than 0%.

3. testfaultinj_udp.sh ->tc03_network_packet_loss_injection.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet loss of 5% is injected in confusion container using netem.Then the UDP packet loss is checked in confusion and orderly respectively using `iperf3`.If the packet loss in orderly comes out to be more than 0% then we conclude that interference is present.

4. testfaultinj_udp.sh ->tc04_network_packet_delay_injection.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet delay of 700ms is injected in confusion container using netem.Then the UDP packet delay is checked in confusion and orderly respectively using `qperf`.If the packet delay in orderly comes out to be more than 5% of what in confusion then we conclude that interference is present.For doing this comparison the values are bought to common unit `us`.

5. testfaultinj_tcp.sh ->tc05_network_packet_reordering_tcp.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet reordering fault of `reorder 35% 70%` is injected in confusion container using netem.Then TCP packet loss, packet delay and retransmissions are evaluated.

6. testfaultinj_tcp.sh ->tc06_network_packet_corruption_tcp.fmf:
Three containers running in a vm confusion, orderly and partner:
A packet corruption fault of 10% is injected in confusion container using netem. Then TCP packet loss, packet delay and retransmissions are evaluated.

## The metrics for these tests are collected using :
1. `podman ps --all` for listing containers.
2. `sar` for network statistics.
3. `ping` for timing and packet loss. -ICMP
4. `iperf3` for packet loss. 
5. `qperf` for packet delay. 
6. `mtr` for packet loss in TCP. 