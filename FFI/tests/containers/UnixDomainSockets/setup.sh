#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/../../common.sh

if ! test -x tst_sys_af-unix; then
  echo "Compiling test for system"
  gcc -o tst_sys_af-unix af_unix.c -Wall -Werror
fi
if ! test -x tst_af-unix; then
  echo "Compiling test for containers"
  image=$(build_container_image gcc)
  podman run $CONTAINER_PARAMS $image gcc -o tst_af-unix af_unix.c -Wall -Werror
fi

