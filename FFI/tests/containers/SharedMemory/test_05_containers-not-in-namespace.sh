#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running shm test in orderly"
podman run $CONTAINER_PARAMS -d --name orderly $BASE_CONTAINER_IMAGE ./tst_shm > /dev/null

printf "%s\n" "-- Running shm test in confusion"
podman run $CONTAINER_PARAMS --name confusion $BASE_CONTAINER_IMAGE ./tst_shm > /dev/null

